# snapcore-mkrpmdistcoresnap

This tool utilizes [DNF](http://dnf.baseurl.org) and [distribution-gpg-keys](https://github.com/xsuchy/distribution-gpg-keys) to create core snaps for RPM-based distributions.

Currently, the tool supports the following distributions:

* Fedora
* Mageia
* openSUSE Leap